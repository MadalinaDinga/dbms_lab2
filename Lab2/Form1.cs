﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace Lab2
{
    public partial class Form1 : Form
    {
        SqlConnection conexiune;
        DataSet ds;
        SqlDataAdapter da;

        //Binding sources for parent, child
        BindingSource bsC; 
        BindingSource bsP;

        List<TextBox> textBoxList;
        List<Label> labelList;
        int idx = 0;

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// set dataAdapter
        /// </summary>
        public void fillDataSet()
        {
            conexiune.Open();

            string selectcmd = ConfigurationManager.AppSettings["ParentSelectCmd"];
            da.SelectCommand = new SqlCommand(selectcmd, conexiune);
            da.Fill(ds, "ParentTable");

            selectcmd = ConfigurationManager.AppSettings["ChildSelectCmd"];
            da.SelectCommand = new SqlCommand(selectcmd, conexiune);
            da.Fill(ds, "ChildTable");
            conexiune.Close();
        }

        /// <summary>
        /// set bindingSources( bsP, bsC)
        /// </summary>
        public void getData()
        {
            string parentId = ConfigurationManager.AppSettings["idParent"];

            //DataRelation= parent-child relationship between two DataTable objects
            DataRelation relation = new DataRelation("Parent_Child_FK",
                ds.Tables["ParentTable"].Columns[parentId],
                ds.Tables["ChildTable"].Columns[parentId]);
            ds.Relations.Add(relation);

            bsP.DataSource = ds;
            bsP.DataMember = "ParentTable";

            bsC.DataSource = bsP;
            bsC.DataMember = "Parent_Child_FK";
        }

        /// <summary>
        /// populate dataGridViews( for child and parent)
        /// </summary>
        public void populateDataGridViews()
        {
            parentDataGridView.DataSource = bsP;
            childDataGridView.DataSource = bsC;
            getData();
            childDataGridView.AutoResizeColumns();
            parentDataGridView.AutoGenerateColumns = true;
        }

        /// <summary>
        /// When the form loads:
        ///     set sqlConnection using ConfigurationManager
        ///     fill dataSet
        ///     populate gridViews( for parent and child)
        ///     set panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        private void Form1_Load(object sender, EventArgs e)
        {
            ConnectionStringSettings conSet = ConfigurationManager.ConnectionStrings["myConn"];
            string connectionString = conSet.ConnectionString;
            conexiune = new SqlConnection(connectionString);

            bsP = new BindingSource();
            bsC = new BindingSource();
            da = new SqlDataAdapter();
            ds = new DataSet();

            fillDataSet();
            populateDataGridViews();
            setUpTextBoxAndLabels();
        }

        /// <summary>
        ///  set panel( labels and textBoxes)
        /// </summary>
        private void setUpTextBoxAndLabels()
        {
            idx = 0;
            textBoxList = new List<TextBox>();
            labelList = new List<Label>();

            //using System.Linq; for .OfType( filters the elements of a System.Collections.IEnumerable based on a specified type
            foreach (Control item in panel1.Controls.OfType<TextBox>())
            {
                panel1.Controls.Remove(item);
            }
            foreach (Control item in panel1.Controls.OfType<Label>())
            {
                panel1.Controls.Remove(item);
            }
            int columnNr = ds.Tables["ChildTable"].Columns.Count;

            for (int i = 0; i < columnNr - 1; i++)
            {
                Label label = new Label();
                label.Text = ds.Tables["ChildTable"].Columns[i].ColumnName;

                //using System.Drawing
                Point textP = new Point(idx * 120, 44);
                Point lableP = new Point(idx * 120, 30);
                label.Location = lableP;
                label.AutoSize = true;

                TextBox textBox = new TextBox();
                textBox.Location = textP;

                textBoxList.Add(textBox);
                labelList.Add(label);
                idx++;

                panel1.Controls.Add(label);
                panel1.Controls.Add(textBox);
            }
        }

        /// <summary>
        /// add given child to parent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBtn_Click(object sender, EventArgs e)
        {            
            conexiune.Open();
            string value;
            int i;
            int selectedRowindex = parentDataGridView.SelectedCells[0].RowIndex;

            //get parentId
            string foreignID = parentDataGridView.Rows[selectedRowindex].Cells[0].Value.ToString();

            string insertCmd = ConfigurationSettings.AppSettings["ChildInsertCmd"];

            da.InsertCommand = new SqlCommand(insertCmd, conexiune);

            int nrColumns = ds.Tables["ChildTable"].Columns.Count;

            for (i = 0; i < nrColumns - 1; i++)
            {
                value = "@value" + (i + 1).ToString();
                da.InsertCommand.Parameters.Add(value, SqlDbType.VarChar).Value = textBoxList[i].Text;
            }

            //parameter value for parentId
            value = "@value" + (i + 1).ToString();
            da.InsertCommand.Parameters.Add(value, SqlDbType.VarChar).Value = foreignID;

            da.InsertCommand.ExecuteNonQuery();

            ds.Tables["ChildTable"].Clear();
            string selectcmd = ConfigurationManager.AppSettings["ChildSelectCmd"];
            da.SelectCommand = new SqlCommand(selectcmd, conexiune);
            da.Fill(ds, "ChildTable");

            conexiune.Close();
        }


        /// <summary>
        /// remove a child by childId
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeBtn_Click(object sender, EventArgs e)
        {
            if (childDataGridView.SelectedCells.Count > 0)
            {
                conexiune.Open();
                int selectedrowindex = childDataGridView.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = childDataGridView.Rows[selectedrowindex];

                //get childId
                string toDeleteId = Convert.ToString(selectedRow.Cells[0].Value);

                string deleteCmd = ConfigurationSettings.AppSettings["ChildDeleteCmd"];
                da.DeleteCommand = new SqlCommand(deleteCmd, conexiune);

                da.DeleteCommand.Parameters.Add("value", SqlDbType.VarChar).Value = toDeleteId;

                da.DeleteCommand.ExecuteNonQuery();
                ds.Tables["ChildTable"].Clear();
                string selectcmd = ConfigurationManager.AppSettings["ChildSelectCmd"];
                da.SelectCommand = new SqlCommand(selectcmd, conexiune);
                da.Fill(ds, "ChildTable");

                conexiune.Close();
            }
            else
                MessageBox.Show("Select a line");
        }

        /// <summary>
        /// update a child by childId, with info from the testBoxList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateBtn_Click(object sender, EventArgs e)
        {
            if (childDataGridView.SelectedCells.Count > 0)
            {
                conexiune.Open();
                string updateCmd = ConfigurationSettings.AppSettings["ChildUpdateCmd"];
                da.UpdateCommand = new SqlCommand(updateCmd, conexiune);

                //get childId
                string childId = textBoxList[0].Text;

                int index = ds.Tables["ChildTable"].Columns.Count;
                int i ;
                string value;                

                for (i = 1; i < index - 1; i++)
                {
                    value = "@value" + i.ToString();
                    da.UpdateCommand.Parameters.Add(value, SqlDbType.VarChar).Value = textBoxList[i].Text;
                }
                
                //value for childId parameter
                da.UpdateCommand.Parameters.Add("value", SqlDbType.VarChar).Value = childId;

                da.UpdateCommand.ExecuteNonQuery();

                ds.Tables["ChildTable"].Clear();
                string selectcmd = ConfigurationManager.AppSettings["ChildSelectCmd"];
                da.SelectCommand = new SqlCommand(selectcmd, conexiune);
                da.Fill(ds, "ChildTable");

                conexiune.Close();
            }

        }
    }

}
